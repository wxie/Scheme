;;; print a diamond with diagonal line of N and using the input character C.

(use-modules (ice-9 format))

(display "Please input the diagonal line length: ")
(define n (read))  ; read diagonal line length
(display "Please input the character to draw: ")
(define c (read))  ; read character to draw)
(display "Now draw the diamond.")
(newline)

(let iterate-1 ((i n))
  (begin
    (format #t "~v_" i)
    (let iterate-2 ((j (- n i)))
      (begin
        (format #t "~a" c)
        (if (> j 0)
            (iterate-2 (1- j)))))
    (newline)
    (if (> i 0)
        (iterate-1 (1- i)))))
