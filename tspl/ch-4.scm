;;;; chapter 4

(define make-list
  (case-lambda
    [(n) (make-list n #f)]
    [(n x)
     (do ([n n (- n 1)] [ls '() (cons x ls)])
         ((zero? n) ls))]))

(define substring1
  (case-lambda
    [(s) (substring1 s 0 (string-length s))]
    [(s start) (substring1 s start (string-length s))]
    [(s start end) (substring s start end)]))

(define-syntax let
  (syntax-rules ()
    [(_ ((x e) ...) b1 b2 ...)
     ((lambda (x ...) b1 b2 ...) e ...)]))

(define-syntax let*
  (syntax-rules ()
    [(_ () e1 e2 ...)
     (let () e1 e2 ...)]
    [(_ ((x1 v1) (x2 v2) ...) e1 e2 ...)
     (let ((x1 v1))
       (let* ((x2 v2) ...) e1 e2 ...))]))

;;  (letrec ((var expr) ...) body1 body2 ...)
(define-syntax letrec
  (syntax-rules ()
    [(_ ((var expr) ...) body1 body2 ...)
     (let ((var #f) ...)
       (let ((temp expr) ...)
         (set! var temp) ...
         (let () body1 body2 ...)))]))

;; (letrec* ((var expr) ...) body1 body2 ...)
(define-syntax letrec*
  (syntax-rules ()
    [(_ ((var expr) ...) body1 body2 ...)
     (let ((var #f) ...)
       (set! var expr) ...
       (let () body1 body2 ...))]))

(define-syntax multi-define-syntax
  (syntax-rules ()
    [(_ (var expr) ...)
     (begin
       (define-syntax var expr)
       ...)]))

(define flip-flop
  (let ([state #f])
    (lambda ()
      (set! state (not state))
      state)))

(define memoize
  (lambda (proc)
    (let ([cache '()])
      (lambda (x)
        (cond
         [(assq x cache) => cdr]
         [else
          (let ([ans (proc x)])
            (set! cache (cons (cons x ans) cache))
            ans)])))))

(define fibonacci
  (memoize
   (lambda (n)
     (if (< n 2)
         1
         (+ (fibonacci (- n 1)) (fibonacci (- n 2)))))))
