;;;; chapter 11 of tspl4

  (define (try thunk)
    (call/cc
      (lambda (k)
        (with-exception-handler
          (lambda (x) (if (error? x) (k #f) (raise x)))
          thunk))))

  (with-exception-handler
    (lambda (x)
      (raise
        (apply condition
          (make-message-condition "oops")
          (simple-conditions x))))
    (lambda ()
      (try (lambda () (raise (make-violation))))))

  (define-syntax try
    (syntax-rules ()
      [(_ e1 e2 ...)
       (guard (x [(error? x) #f]) e1 e2 ...)]))
  (define open-one
    (lambda fn*
      (let loop ([ls fn*])
        (if (null? ls)
            (error 'open-one "all open attempts failed" fn*)
            (or (try (open-input-file (car ls)))
                (loop (cdr ls)))))))
  ; say bar.ss exists but not foo.ss:
  (open-one "foo.ss" "bar.ss")
