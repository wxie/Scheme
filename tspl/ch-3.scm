;;;; chapter 3 of tspl
;; library
(library (grades)
  (export gpa->grade gpa)
  (import (rnrs))

  (define in-range?
    (lambda (x n y)
      (and (>= n x) (< n y))))

  (define-syntax range-case
    (syntax-rules (- else)
      [(_ expr ((x - y) e1 e2 ...) ... [else ee1 ee2 ...])
       (let ([tmp expr])
         (cond
          [(in-range? x tmp y) e1 e2 ...]
          ...
          [else ee1 ee2 ...]))]
      [(_ expr ((x - y) e1 e2 ...) ...)
       (let ([tmp expr])
         (cond
          [(in-range? x tmp y) e1 e2 ...]
          ...))]))

  (define letter->number
    (lambda (x)
      (case x
        [(a)  4.0]
        [(b)  3.0]
        [(c)  2.0]
        [(d)  1.0]
        [(f)  0.0]
        [else (assertion-violation 'grade "invalid letter grade"
                                   x)])))

  (define gpa->grade
    (lambda (x)
      (range-case x
                  [(0.0 - 0.5) 'f]
                  [(0.5 - 1.5) 'd]
                  [(1.5 - 2.5) 'c]
                  [(2.5 - 3.5) 'b]
                  [else 'a])))

  (define-syntax gpa
    (syntax-rules ()
      [(_ g1 g2 ...)
       (let ([ls (map letter->number '(g1 g2 ...))])
         (/ (apply + ls) (length ls)))]))

  (define-syntax gpa
    (syntax-rules ()
      [(_ g1 g2 ...)
       (let ([ls (map letter->number (remq 'x '(g1 g2 ...)))])
         (if (null? ls)
             'x
             (/ (apply + ls) (length ls))))])))

;;(import (grades))
(gpa c a c b b)   ; 2.8
(gpa->grade 2.8)  ; b

(define $distribution
  (lambda (ls)
    (let loop ([ls ls] [a 0] [b 0] [c 0] [d 0] [f 0])
      (if (null? ls)
          (list (list a 'a) (list b 'b) (list c 'c)
                (list d 'd) (list f 'f))
          (case (car ls)
            [(a) (loop (cdr ls) (+ a 1) b c d f)]
            [(b) (loop (cdr ls) a (+ b 1) c d f)]
            [(c) (loop (cdr ls) a b (+ c 1) d f)]
            [(d) (loop (cdr ls) a b c (+ d 1) f)]
            [(f) (loop (cdr ls) a b c d (+ f 1))]
                                        ; ignore x grades, per preceding exercise
            [(x) (loop (cdr ls) a b c d f)]
            [else (assertion-violation 'distribution
                                       "unrecognized grade letter"
                                       (car ls))])))))
(define-syntax distribution
  (syntax-rules ()
    [(_ g1 g2 ...)
     ($distribution '(g1 g2 ...))]))

(define histogram
  (lambda (port distr)
    (for-each
     (lambda (n g)
       (put-datum port g)
       (put-string port ": ")
       (let loop ([n n])
         (unless (= n 0)
           (put-char port #\*)
           (loop (- n 1))))
       (put-string port "\n"))
     (map car distr)
     (map cadr distr))))

(histogram
 (current-output-port)
 (distribution a b a c c a c a f b a))

(define list?
  (lambda (x)
    (letrec ([race
              (lambda (h t)
                (if (pair? h)
                    (let ([h (cdr h)])
                      (if (pair? h)
                          (and (not (eq? h t))
                               (race (cdr h) (cdr t)))
                          (null? h)))
                    (null? h)))])
      (race x x))))

(define list?
  (lambda (x)
    (let race ([h x] [t x])
      (if (pair? h)
          (let ([h (cdr h)])
            (if (pair? h)
                (and (not (eq? h t))
                     (race (cdr h) (cdr t)))
                (null? h)))
          (null? h)))))

(define factor
  (lambda (n)
    (let f ([n n] [i 2])
      (cond
       [(>= i n) (list n)]
       [(integer? (/ n i))
        (cons i (f (/ n i) i))]
       [else (f n (+ i 1))]))))

(define product
  (lambda (ls)
    (call/cc
     (lambda (break)
       (let f ([ls ls])
         (cond
          [(null? ls) 1]
          [(= (car ls) 0) (break 0)]
          [else (* (car ls) (f (cdr ls)))]))))))

(define calc #f)
(let ()
  (define do-calc
    (lambda (ek expr)
      (cond
       [(number? expr) expr]
       [(and (list? expr) (= (length expr) 3))
        (let ([op (car expr)] [args (cdr expr)])
          (case op
            [(add) (apply-op ek + args)]
            [(sub) (apply-op ek - args)]
            [(mul) (apply-op ek * args)]
            [(div) (apply-op ek / args)]
            [else (complain ek "invalid operator" op)]))]
       [else (complain ek "invalid expression" expr)])))
  (define apply-op
    (lambda (ek op args)
      (op (do-calc ek (car args)) (do-calc ek (cadr args)))))
  (define complain
    (lambda (ek msg expr)
      (ek (list msg expr))))
  (set! calc
        (lambda (expr)
                                        ; grab an error continuation ek
          (call/cc
           (lambda (ek)
             (do-calc ek expr))))))

(calc '(add (mul 3 2) -4))

;; light weight process
(define lwp-list '())
(define lwp
  (lambda (thunk)
    (set! lwp-list (append lwp-list (list thunk)))))
(define start
  (lambda ()
    (call/cc
     (lambda (k)
       (set! quit-k k)
       (next)))))
(define next
  (lambda ()
    (let ([p (car lwp-list)])
      (set! lwp-list (cdr lwp-list))
      (p))))
(define pause
  (lambda ()
    (call/cc
     (lambda (k)
       (lwp (lambda () (k #f)))
       (next)))))
(define quit
  (lambda (v)
    (if (null? lwp-list)
        (quit-k v)
        (next))))

(define make-queue
  (lambda ()
    (let ([end (cons 'ignored '())])
      (cons end end))))

(define lwp-queue (make-queue))
(define lwp
  (lambda (thunk)
    (putq! lwp-queue thunk)))
(define start
  (lambda ()
    (let ([p (getq lwp-queue)])
      (delq! lwp-queue)
      (p))))
(define pause
  (lambda ()
    (call/cc
     (lambda (k)
       (lwp (lambda () (k #f)))
       (start)))))

;; calc
(define calc
  (lambda (expr)
    (call/cc
     (lambda (ek)
       (define do-calc
         (lambda (expr)
           (cond
            [(number? expr) expr]
            [(and (list? expr) (= (length expr) 3))
             (let ([op (car expr)] [args (cdr expr)])
               (case op
                 [(add) (apply-op + args)]
                 [(sub) (apply-op - args)]
                 [(mul) (apply-op * args)]
                 [(div) (apply-op / args)]
                 [else (complain "invalid operator" op)]))]
            [else (complain "invalid expression" expr)])))
       (define apply-op
         (lambda (op args)
           (op (do-calc (car args)) (do-calc (cadr args)))))
       (define complain
         (lambda (msg expr)
           (ek (list msg expr))))
       (do-calc expr)))))

(define calc #f)
(let ()
  (define do-calc
    (lambda (expr)
      (cond
       [(number? expr) expr]
       [(and (list? expr) (= (length expr) 3))
        (let ([op (car expr)] [args (cdr expr)])
          (case op
            [(add) (apply-op + args)]
            [(sub) (apply-op - args)]
            [(mul) (apply-op * args)]
            [(div) (apply-op / args)]
            [else (complain "invalid operator" op)]))]
       [else (complain "invalid expression" expr)])))
  (define apply-op
    (lambda (op args)
      (op (do-calc (car args)) (do-calc (cadr args)))))
  (define complain
    (lambda (msg expr)
      (assertion-violation 'calc msg expr)))
  (set! calc
        (lambda (expr)
          (do-calc expr))))

(let ()
  (define do-calc
    (lambda (ek expr)
      (cond
       [(number? expr) expr]
       [(and (list? expr) (= (length expr) 2))
        (let ([op (car expr)] [args (cdr expr)])
          (case op
            [(minus) (apply-op1 ek - args)]
            [(sqrt) (apply-op1 ek sqrt args)]
            [else (complain ek "invalid unary operator" op)]))]
       [(and (list? expr) (= (length expr) 3))
        (let ([op (car expr)] [args (cdr expr)])
          (case op
            [(add) (apply-op2 ek + args)]
            [(sub) (apply-op2 ek - args)]
            [(mul times) (apply-op2 ek * args)]
            [(div) (apply-op2 ek / args)]
            [(expt) (apply-op2 ek expt args)]
            [else (complain ek "invalid binary operator" op)]))]
       [else (complain ek "invalid expression" expr)])))
  (define apply-op1
    (lambda (ek op args)
      (op (do-calc ek (car args)))))
  (define apply-op2
    (lambda (ek op args)
      (op (do-calc ek (car args)) (do-calc ek (cadr args)))))
  (define complain
    (lambda (ek msg expr)
      (ek (list msg expr))))
  (set! calc
        (lambda (expr)
          (call/cc
           (lambda (ek)
             (do-calc ek expr))))))
