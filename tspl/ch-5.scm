;;;; chapter 5 of tspl4
(library (mrvs)
  (export call-with-values values call/cc
          (rename (call/cc call-with-current-continuation)))
  (import
   (rename
    (except (rnrs) values call-with-values)
    (call/cc rnrs:call/cc)))

  (define magic (cons 'multiple 'values))

  (define magic?
    (lambda (x)
      (and (pair? x) (eq? (car x) magic))))

  (define call/cc
    (lambda (p)
      (rnrs:call/cc
       (lambda (k)
         (p (lambda args
              (k (apply values args))))))))

  (define values
    (lambda args
      (if (and (not (null? args)) (null? (cdr args)))
          (car args)
          (cons magic args))))

  (define call-with-values
    (lambda (producer consumer)
      (let ([x (producer)])
        (if (magic? x)
            (apply consumer (cdr x))
            (consumer x))))))

(library (dynamic-wind)
  (export dynamic-wind call/cc
          (rename (call/cc call-with-current-continuation)))
  (import (rename (except (rnrs) dynamic-wind) (call/cc
                                                rnrs:call/cc)))

  (define winders '())

  (define common-tail
    (lambda (x y)
      (let ([lx (length x)] [ly (length y)])
        (do ([x (if (> lx ly) (list-tail x (- lx ly)) x) (cdr x)]
             [y (if (> ly lx) (list-tail y (- ly lx)) y) (cdr y)])
            ((eq? x y) x)))))

  (define do-wind
    (lambda (new)
      (let ([tail (common-tail new winders)])
        (let f ([ls winders])
          (if (not (eq? ls tail))
              (begin
                (set! winders (cdr ls))
                ((cdar ls))
                (f (cdr ls)))))
        (let f ([ls new])
          (if (not (eq? ls tail))
              (begin
                (f (cdr ls))
                ((caar ls))
                (set! winders ls)))))))

  (define call/cc
    (lambda (f)
      (rnrs:call/cc
       (lambda (k)
         (f (let ([save winders])
              (lambda (x)
                (unless (eq? save winders) (do-wind save))
                (k x))))))))

  (define dynamic-wind
    (lambda (in body out)
      (in)
      (set! winders (cons (cons in out) winders))
      (let-values ([ans* (body)])
        (set! winders (cdr winders))
        (out)
        (apply values ans*)))))

(define-syntax when
  (syntax-rules ()
    [(_ e0 e1 e2 ...)
     (if e0 (begin e1 e2 ...))]))

(define-syntax unless
  (syntax-rules ()
    [(_ e0 e1 e2 ...)
     (if (not e0) (begin e1 e2 ...))]))

(define-syntax unless
  (syntax-rules ()
    [(_ e0 e1 e2 ...)
     (when (not e0) e1 e2 ...)]))

(define divisors
  (lambda (n)
    (let f ([i 2] [ls '()])
      (cond
       [(>= i n) ls]
       [(integer? (/ n i)) (f (+ i 1) (cons i ls))]
       [else (f (+ i 1) ls)]))))

(define divisors
  (lambda (n)
    (do ([i 2 (+ i 1)]
         [ls '()
             (if (integer? (/ n i))
                 (cons i ls)
                 ls)])
        ((>= i n) ls))))

(define map
  (lambda (f ls . more)
    (if (null? more)
        (let map1 ([ls ls])
          (if (null? ls)
              '()
              (cons (f (car ls))
                    (map1 (cdr ls)))))
        (let map-more ([ls ls] [more more])
          (if (null? ls)
              '()
              (cons
               (apply f (car ls) (map car more))
               (map-more (cdr ls) (map cdr more))))))))
(define for-each
  (lambda (f ls . more)
    (do ([ls ls (cdr ls)] [more more (map cdr more)])
        ((null? ls))
      (apply f (car ls) (map car more)))))

(define exists
  (lambda (f ls . more)
    (and (not (null? ls))
         (let exists ([x (car ls)] [ls (cdr ls)] [more more])
           (if (null? ls)
               (apply f x (map car more))
               (or (apply f x (map car more))
                   (exists (car ls) (cdr ls) (map cdr more))))))))

(define for-all
  (lambda (f ls . more)
    (or (null? ls)
        (let for-all ([x (car ls)] [ls (cdr ls)] [more more])
          (if (null? ls)
              (apply f x (map car more))
              (and (apply f x (map car more))
                   (for-all (car ls) (cdr ls) (map cdr more))))))))

(define member
  (lambda (x ls)
    (call/cc
     (lambda (break)
       (do ([ls ls (cdr ls)])
           ((null? ls) #f)
         (when (equal? x (car ls))
           (break ls)))))))

(define-syntax unwind-protect
  (syntax-rules ()
    [(_ body cleanup ...)
     (dynamic-wind
       (lambda () #f)
       (lambda () body)
       (lambda () cleanup ...))]))

((call/cc
  (let ([x 'a])
    (lambda (k)
      (unwind-protect
       (k (lambda () x))
       (set! x 'b))))))

(define-syntax fluid-let
  (syntax-rules ()
    [(_ ((x e)) b1 b2 ...)
     (let ([y e])
       (let ([swap (lambda () (let ([t x]) (set! x y) (set! y t)))])
         (dynamic-wind swap (lambda () b1 b2 ...) swap)))]))

(let ([x 'a])
  (let ([f (lambda () x)])
    (cons (call/cc
           (lambda (k)
             (fluid-let ([x 'b])
               (k (f)))))
          (f))))

(define reenter #f)
(define x 0)
(fluid-let ([x 1])
  (call/cc (lambda (k) (set! reenter k)))
  (set! x (+ x 1))
  x)

(define stream-car
  (lambda (s)
    (car (force s))))

(define stream-cdr
  (lambda (s)
    (cdr (force s))))

(define counters
  (let next ([n 1])
    (delay (cons n (next (+ n 1))))))

(stream-car counters)

(define stream-add
  (lambda (s1 s2)
    (delay (cons
            (+ (stream-car s1) (stream-car s2))
            (stream-add (stream-cdr s1) (stream-cdr s2))))))

(define even-counters
  (stream-add counters counters))

(stream-car even-counters)

(define-syntax delay
  (syntax-rules ()
    [(_ expr) (make-promise (lambda () expr))]))

(define make-promise
  (lambda (p)
    (let ([val #f] [set? #f])
      (lambda ()
        (unless set?
          (let ([x (p)])
            (unless set?
              (set! val x)
              (set! set? #t))))
        val))))

(define force
  (lambda (promise)
    (promise)))

(define make-promise
  (lambda (p)
    (let ([vals #f] [set? #f])
      (lambda ()
        (unless set?
          (call-with-values p
            (lambda x
              (unless set?
                (set! vals x)
                (set! set? #t)))))
        (apply values vals)))))

(define p (delay (values 1 2 3)))
(force p)

(call-with-values (lambda () (force p)) +)

(define-record-type promise
  (fields (immutable p) (mutable vals) (mutable set?))
  (protocol (lambda (new) (lambda (p) (new p #f #f)))))

(define force
  (lambda (promise)
    (unless (promise? promise)
      (assertion-violation 'promise "invalid argument" promise))
    (unless (promise-set? promise)
      (call-with-values (promise-p promise)
        (lambda x
          (unless (promise-set? promise)
            (promise-vals-set! promise x)
            (promise-set?-set! promise #t)))))
    (apply values (promise-vals promise))))

(define segment-length
  (lambda (p1 p2)
    (call-with-values
        (lambda () (dxdy p1 p2))
      (lambda (dx dy) (sqrt (+ (* dx dx) (* dy dy)))))))

(define segment-slope
  (lambda (p1 p2)
    (call-with-values
        (lambda () (dxdy p1 p2))
      (lambda (dx dy) (/ dy dx)))))

(segment-length '(1 . 4) '(4 . 8))

(define describe-segment
  (lambda (p1 p2)
    (call-with-values
        (lambda () (dxdy p1 p2))
      (lambda (dx dy)
        (values
         (sqrt (+ (* dx dx) (* dy dy)))
         (/ dy dx))))))

(describe-segment '(1 . 4) '(4 . 8))

(define split
  (lambda (ls)
    (if (or (null? ls) (null? (cdr ls)))
        (values ls '())
        (call-with-values
            (lambda () (split (cddr ls)))
          (lambda (odds evens)
            (values (cons (car ls) odds)
                    (cons (cadr ls) evens)))))))

(split '(a b c d e f))

(call-with-values
    (lambda ()
      (call/cc (lambda (k) (k 2 3))))
  (lambda (x y) (list x y)))

(define-syntax first
  (syntax-rules ()
    [(_ expr)
     (call-with-values
         (lambda () expr)
       (lambda (x . y) x))]))

(if (first (values #t #f)) 'a 'b)

(define-syntax with-values
  (syntax-rules ()
    [(_ expr consumer)
     (call-with-values (lambda () expr) consumer)]))

(define call-with-port
  (lambda (port proc)
    (let-values ([val* (proc port)])
      (close-port port)
      (apply values val*))))

(define call-with-port
  (lambda (port proc)
    (call-with-values (lambda () (proc port))
      (case-lambda
        [(val) (close-port port) val]
        [val* (close-port port) (apply values val*)]))))
