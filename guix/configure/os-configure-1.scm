;;;; _*_ mode: scheme; _*_
;;;; This is an operating system configuration template
;;;; for a "desktop" setup with GNOME and Xfce where the
;;;; root partition is encrypted with LUKS, and a swap file.
;;;; https://forum.systemcrafters.net/t/how-to-get-rid-of-original-tty2/882/7

(use-modules (gnu)
	     (srfi srfi-1)
	     (ice-9 match)
             (gnu system)
             (gnu system nss)
             (guix utils)
             (gnu packages linux)
             (gnu packages audio)
             (gnu packages networking)
             (gnu packages password-utils)
             (gnu packages version-control)
             (gnu packages ssh)
             (gnu packages tls)
             (gnu packages cryptsetup)
             (gnu packages cpio)
             (gnu packages textutils)
             (gnu packages firmware)
	     (gnu packages gnupg)
	     (gnu packages gnuzilla)
             (gnu packages texinfo)
             (gnu packages compression)
	     (gnu services cups)
             (gnu services shepherd)
	     ;; Import nonfree linux module.
             (nongnu packages linux)
             (nongnu system linux-initrd)
	     ;; (nongnu packages mozilla)
             (nongnu packages firmware))

(use-service-modules
             desktop
             sddm
             xorg
             dbus
             ssh)

(use-package-modules
             certs
             kde)


(define install-grub-efi-mkimage
  #~(lambda (bootloader efi-dir mount-point)
  "Create an Grub EFI image with included cryptomount support for luks2,
which grub-install does not handle yet."
        (when efi-dir
            (let ((grub-mkimage (string-append bootloader "/bin/grub-mkimage"))
                  ;; Required modules, YMMV.
                  (modules (list "luks2" "part_gpt" "cryptodisk" "gcry_rijndael" "pbkdf2" "gcry_sha256" "btrfs"))
                  (prefix (string-append mount-point "/boot/grub"))
                  ;; Different configuration required to set up a crypto
                  ;; device. Change crypto_uuid to match your output of
                  ;; `cryptsetup luksUUID /device` but remove the dashes.
                  ;; XXX: Maybe cryptomount -a could work?
                  (config #$(plain-file "grub.cfg" "set crypto_uuid=d3267bbc2a1d49f6a8e9a7bfc8c750e3
cryptomount -u $crypto_uuid
set root=crypto0
set prefix=($root)/boot/grub
insmod normal
normal"))
                  (target-esp (if (file-exists? (string-append mount-point efi-dir))
                                  (string-append mount-point efi-dir)
                                  efi-dir)))
              (apply invoke (append
                             (list
                               grub-mkimage
                              "-p" prefix
                              "-O" "x86_64-efi"
                              "-c" config
                              "-o" (string-append target-esp "/EFI/Guix/grubx64.efi"))
                             modules))))))

(define grub-efi-bootloader-luks2
  (bootloader
    (inherit grub-efi-bootloader)
    (name 'grub-efi-luks2)
    (installer install-grub-efi-mkimage)))

(define %issue
  ;; Greeting.
  "
\x1b[1;37mWelcome to Guix \\s \\r \\v \\n \\l \x1b[0m

\x1b[1;33mUse Alt-F2 for documentation.\x1b[0m
")

(operating-system
  (kernel linux)
  (initrd microcode-initrd)
  (firmware (list linux-firmware))
  ;(firmware (cons*
                   ; amdgpu-firmware
                   ; atheros-firmware
                   ; %base-firmware))
  (host-name "X13")
  (timezone "Europe/Paris")
  (locale "de_CH.utf8")

  ;; Choose US English keyboard layout.  The "altgr-intl"
  ;; variant provides dead keys for accented characters.
  ;(keyboard-layout (keyboard-layout "us" "altgr-intl"))
  (keyboard-layout (keyboard-layout "ch"))

  ;; Use the UEFI variant of GRUB with the EFI System
  ;; Partition mounted on /boot/efi.
  ;; /root in /root/key-file.cpio refers to the
  ;; /dev/mapper/env/root subvolume and not the home of root.
  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader-luks2)
                (targets '("/boot/efi"))
                (keyboard-layout keyboard-layout)
                (extra-initrd "/root/key-file.cpio")))

  ;; Specify a mapped device for the encrypted root partition.
  ;; The UUID is that returned by 'cryptsetup luksUUID'.
  (mapped-devices
   (list (mapped-device
          (source (uuid "d3267bbc-2a1d-49f6-a8e9-a7bfc8c750e3"))
          (target "enc")
          (type (luks-device-mapping-with-options #:key-file "/key-file.bin")))))

  (file-systems (cons* (file-system
                         (device "/dev/mapper/enc")
                         (mount-point "/")
                         (type "btrfs")
                         (options "subvol=root")
                         (dependencies mapped-devices))
                       (file-system
                         (device "/dev/mapper/enc")
                         (mount-point "/boot")
                         (type "btrfs")
                         (options "subvol=boot")
                         (dependencies mapped-devices))
                       (file-system
                         (device "/dev/mapper/enc")
                         (mount-point "/gnu")
                         (type "btrfs")
                         (options "subvol=gnu")
                         (dependencies mapped-devices))
                       (file-system
                         (device "/dev/mapper/enc")
                         (mount-point "/home")
                         (type "btrfs")
                         (options "subvol=home")
                         (dependencies mapped-devices))
                       (file-system
                         (device "/dev/mapper/enc")
                         (mount-point "/var/log")
                         (type "btrfs")
                         (options "subvol=log")
                         (dependencies mapped-devices))
                       (file-system
                         (device "/dev/mapper/enc")
                         (mount-point "/data")
                         (type "btrfs")
                         (options "subvol=data")
                         (dependencies mapped-devices))
                       (file-system
                         (device "/dev/mapper/enc")
                         (mount-point "/swap")
                         (type "btrfs")
                         (options "subvol=swap")
                         (dependencies mapped-devices))
                       (file-system
                         (mount-point "/boot/efi")
                         (type "vfat")
                         (device (uuid "9311-566C" 'fat32)))
                 %base-file-systems))
  (swap-devices
    (list
      (swap-space
        (target "/swap/swapfile")
        (dependencies (filter (file-system-mount-point-predicate "/swap")
                              file-systems)))))

  (users (cons* (user-account
                  (name "dgr")
                  (comment "Dominik Riva - owner of this machine")
                  (password #f)
                  (group "students")
                  (supplementary-groups '("wheel" "netdev"
                                          "audio" "video")))
		(user-account
		 (name "info")
		 (group "nogroup")
		 (shell (file-append info-reader "/bin/info"))
		 (comment "auto-login user for info-reader"))
               %base-user-accounts))

  ;; Add the `students' group
  (groups (cons* (user-group
                  (name "students"))
                 %base-groups))

  ;; This is where we specify system-wide packages.
  (packages (cons* bluez
                   bluez-alsa
                   blueman
		   icecat
		   gpgme ;; for kwallet
                   keepassxc
                   git
                   ;gvfs ;; for user mounts
                   openssh
                   openssl
                   cryptsetup
                   cpio
		   aha ;; for plasma info center firmware security
                   fwupd ;; also for firmware security
                   %base-packages))

  ;; inject the modified issue to get stuff like TTY N° displayed
  (issue %issue)

  ;; Use the "desktop" services, which
  ;; include the X11 log-in service, networking with
  ;; NetworkManager, and more.
  (services
   (append (list (service plasma-desktop-service-type)
                 ;; To facilitate copy/paste.
                 (service gpm-service-type)
                 ;;(service fwupd-service-type)
                 (simple-service 'fwupd-polkit
				 polkit-service-type
                                 (list fwupd))
                 (simple-service 'fwupd-dbus
                                 dbus-root-service-type
                                 (list fwupd))
                 (set-xorg-configuration
                  (xorg-configuration
                   (keyboard-layout keyboard-layout))
                  sddm-service-type)
                 (service sddm-service-type
                          (sddm-configuration
                           (theme "breeze")
                           (display-server "wayland")
			   ))
                 (service bluetooth-service-type
                          (bluetooth-configuration
                           (auto-enable? #t)))
                 (simple-service 'dbus-extras
				 dbus-root-service-type
                                 (list blueman))
                 (service openssh-service-type
                          (openssh-configuration
                           (x11-forwarding? #t)
                           (permit-root-login 'prohibit-password)
                           (authorized-keys
                            `(("alice" ,(local-file "alice.pub"))
                              ("bob" ,(local-file "bob.pub"))))))
		 (service cups-service-type))
		 (modify-services %desktop-services
				  ;; modify TTY2 to get info for quick references if I hose the GUI
				  (mingetty-service-type
				   config =>
				   (mingetty-configuration
				    (inherit config)
				    (auto-login
				     (match (mingetty-configuration-tty config)
				       ("tty2" "info")
				       (_ #f)))))
	    			  (delete gdm-service-type))))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
