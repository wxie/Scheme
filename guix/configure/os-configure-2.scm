(use-modules (gnu)
	     (nongnu packages linux)
             (nongnu packages nvidia)
             (gnu packages gnome)
	     (gnu home services ssh)
	     (gnu services xorg)
	     )

(use-service-modules cups desktop networking ssh xorg)

(operating-system
  (kernel linux)
  (firmware (list linux-firmware))
  (locale "en_US.utf8")
  (timezone "America/Santiago")
  (keyboard-layout (keyboard-layout "us" "altgr-intl"))
  (host-name "zuanphyrus")

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                  (name "juanpablo")
                  (comment "Juanpablo")
                  (group "users")
                  (home-directory "/home/juanpablo")
                  (supplementary-groups '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))

  ;; Packages installed system-wide.  Users can also install packages
  ;; under their own account: use 'guix search KEYWORD' to search
  ;; for packages and 'guix install PACKAGE' to install a package.
  (packages (append (list (specification->package "emacs")
                          (specification->package "emacs-exwm")
                          (specification->package
                           "emacs-desktop-environment")
                          (specification->package "nss-certs"))
                    %base-packages))

  ;; Tweaks for nvidia drivers to work
  (kernel-arguments '("modprobe.blacklist=nouveau"
                      ;; Set this if the card is not used for displaying or
                      ;; you're using Wayland:
                      "nvidia_drm.modeset=1"))

  ;; Below is the list of system services.  To search for available
  ;; services, run 'guix system search KEYWORD' in a terminal.
  (services
   (append (list (service nvidia-service-type)
		 ;; Configure desktop environment, GNOME for example.
		 (service gnome-desktop-service-type
			  ;; Enable NVIDIA support, only do this when the card is
			  ;; used for displaying.
			  (gnome-desktop-configuration
			   (gnome (replace-mesa gnome))))
		 ;; Configure Xorg server, only do this when the card is used for
		 ;; displaying.
		 (set-xorg-configuration
		  (xorg-configuration
		   (modules (cons nvda %default-xorg-modules))
		   (drivers '("nvidia"))))
                 ;; To configure OpenSSH, pass an 'openssh-configuration'
                 ;; record as a second argument to 'service' below.
                 (service openssh-service-type (openssh-configuration))
                 (service tor-service-type)
                 (service cups-service-type)
		 (service bluetooth-service-type)
		 ;; (service home-openssh-service-type
		 ;; 	  (home-openssh-configuration
		 ;; 	   (authorized-keys (list (local-file "toretto.pub")))))
                 (set-xorg-configuration
                  (xorg-configuration (keyboard-layout keyboard-layout)))
		 (simple-service
                     'custom-udev-rules udev-service-type
                     (list nvidia-driver))
                 (service kernel-module-loader-service-type
                          '("ipmi_devintf"
                            "nvidia"
                            "nvidia_modeset"
                            "nvidia_uvm"))
		 )

	   (modify-services %desktop-services
             (guix-service-type config => (guix-configuration
               (inherit config)
               (substitute-urls
                (append (list "https://substitutes.nonguix.org")
                  %default-substitute-urls))
               (authorized-keys
                (append (list (local-file "./signing-key.pub"))
                  %default-authorized-guix-keys)))))
           ))
  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets (list "/boot/efi"))
                (keyboard-layout keyboard-layout)))

  ;; The list of file systems that get "mounted".  The unique
  ;; file system identifiers there ("UUIDs") can be obtained
  ;; by running 'blkid' in a terminal.
  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device (uuid
                                  "80d6001d-add8-49f7-a241-4730ffb98605"
                                  'ext4))
                         (type "ext4"))
                       (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "CA92-A380"
                                       'fat32))
                         (type "vfat")) %base-file-systems)))
