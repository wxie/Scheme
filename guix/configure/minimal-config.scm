;; This is an operating system configuration generated
;; by the graphical installer.
;;
;; Once installation is complete, you can learn and modify
;; this file to tweak the system configuration, and pass it
;; to the 'guix system reconfigure' command to effect your
;; changes.

(define-module (nebk systems minimal-config)
               #:use-module (gnu)
               #:use-module (gnu services)
               #:use-module (guix)
               #:use-module (nongnu packages linux)
               #:use-module (nongnu system linux-initrd))

(use-service-modules linux desktop networking dbus ssh xorg)
(use-package-modules linux fonts glib)

(define %nonguix-substitutes-service
  (simple-service 'add-nonguix-substitutes
                  guix-service-type
                  (guix-extension
                    (substitute-urls
                      (append (list "https://substitutes.nonguix.org")
                              %default-substitute-urls))
                    (authorized-keys
                      (append (list
                                (plain-file
                                  "nonguix.pub"
                                  (string-join 
                                    (list
                                      "(public-key"
                                      " (ecc"
                                      "  (curve Ed25519)"
                                      "  (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))")
                                    "\n")))
                              %default-authorized-guix-keys)))))

(define %thinkpad-services
  (list
    ;; udev rule to fix trackpoint drift
    (udev-rules-service 'trackpoint
                        (udev-rule
                          "60-trackpoint.rules"
                          (string-join
                            (list
                              "ACTION==\"add\""
                              "SUBSYSTEM==\"input\""
                              "ATTR{name}==\"TPPS/2 IBM TrackPoint\""
                              "ATTR{device/drift_time}=\"25\"")
                            ",")))
    ;; setup wifi settings for the thinkpad
    (service kernel-module-loader-service-type '("iwlwifi"))
    (simple-service 'iwlwifi-config
                    etc-service-type
                    (list `("modprobe.d/iwlwifi.conf"
                            ,(plain-file
                               "iwlwifi.conf"
                               (string-join
                                 (list "options iwlwifi 11n_disable=1"
                                       "options iwlwifi swcrypto=1")
                                 "\n")))))))


(operating-system
  (kernel linux)
  ;; add logitech drivers to the initrd
  (initrd-modules (cons*
                    "hid_logitech_hidpp"
                    "hid_logitech_dj"
                    %base-initrd-modules))
  (initrd microcode-initrd) 
  (firmware (cons* iwlwifi-firmware
                   %base-firmware))

  (locale "en_US.utf8")
  (timezone "America/New_York")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "sarah")

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                  (name "nebk")
                  (comment "Sam Ellicott")
                  (group "users")
                  (home-directory "/home/nebk")
                  (supplementary-groups '("wheel"
                                          "netdev"
                                          "input"
                                          "audio"
                                          "video")))
                %base-user-accounts))


  ;; The list of file systems that get "mounted".  The unique
  ;; file system identifiers there ("UUIDs") can be obtained
  ;; by running 'blkid' in a terminal.
  (bootloader (bootloader-configuration
                (bootloader grub-bootloader)
                (targets (list "/dev/sda"))
                (keyboard-layout keyboard-layout)))
  (swap-devices (list (swap-space
                        (target (uuid
                                  "1313ce76-b622-4b1c-9050-03d225e10e2a")))))
  (mapped-devices (list (mapped-device
                          (source (uuid
                                    "4b933aba-ad9a-4bae-b8a0-d50876297e05"))
                          (target "Guix-Home")
                          (type luks-device-mapping))))

  (file-systems (cons* (file-system
                         (mount-point "/home")
                         (device "/dev/mapper/Guix-Home")
                         (type "ext4")
                         (dependencies mapped-devices))
                       (file-system
                         (mount-point "/")
                         (device (uuid
                                   "88214371-1e6c-451e-a99a-4956a47be2b3"
                                   'ext4))
                         (type "ext4"))
                       %base-file-systems))

  ;; Packages installed system-wide.  Users can also install packages
  ;; under their own account: use 'guix search KEYWORD' to search
  ;; for packages and 'guix install PACKAGE' to install a package.
  (packages (cons* (specification->package "i3-wm")
                   (specification->package "i3status")
                   (specification->package "dmenu")
                   (specification->package "xorg-server")
                   (specification->package "st")
                   (specification->package "alacritty")
                   (specification->package "pulseaudio")
                   (specification->package "pasystray")
                   (specification->package "network-manager-applet")
                   (specification->package "vim")

                   ;; XDG utilities
                   (specification->package "xdg-desktop-portal")
                   (specification->package "xdg-utils")
                   (specification->package "xdg-dbus-proxy")
                   (specification->package "shared-mime-info")
                   (list glib "bin")

                   ;; Icon Themes
                   (specification->package "breeze-icons")
                   (specification->package "adwaita-icon-theme")

                   ;; fonts
                   (specification->package "font-liberation")
                   (specification->package "font-awesome")
                   (specification->package "font-dejavu")
                   (specification->package "font-google-noto")
                   (specification->package "font-google-noto-emoji")
                   %base-packages))

  ;; Base services
  ;; copied from:
  ;; https://github.dev/daviwil/dotfiles/blob/master/daviwil/systems/base.scm 
  (services (append
              (modify-services
                %base-services
                (delete login-service-type)
                (delete mingetty-service-type)
                (delete console-font-service-type))
              (cons*
                ;; Configure login prompts
                (service elogind-service-type)
                (service console-font-service-type
                         (map (lambda (tty)
                                (cons tty (file-append
                                            font-terminus
                                            "/share/consolefonts/ter-116n")))
                              '("tty1" "tty2" "tty3")))
                (service greetd-service-type
                         (greetd-configuration
                           (greeter-supplementary-groups
                             (list "video" "input"))
                           (terminals
                             (list
                               (greetd-terminal-configuration
                                 (terminal-vt "1")
                                 (terminal-switch #t))
                               (greetd-terminal-configuration
                                 (terminal-vt "2"))
                               (greetd-terminal-configuration
                                 (terminal-vt "3"))))))

                ;; Network configuration
                (service network-manager-service-type)
                (service wpa-supplicant-service-type)

                ;; non-free substitutes (for drivers mostly)
                %nonguix-substitutes-service

                ;; basic desktop services (cargo culted)
                (service polkit-service-type)
                (service dbus-root-service-type)
                fontconfig-file-system-service

                ;; ntp for time updates
                (service ntp-service-type)

                ;; xorg configuration
                (set-xorg-configuration
                  (xorg-configuration
                    (modules
                      (cons* "xf86-video-nouveau"
                             %default-xorg-modules))))
                (service startx-command-service-type)

                ;; X11 socket directory for for XWayland
                (service x11-socket-directory-service-type)

                (udev-rules-service 'pipewire-add-udev-rules pipewire)
                (udev-rules-service 'brightnessctl-udev-rules brightnessctl)

                ;; stuff related to the thikpad hardware
                %thinkpad-services)))

  (name-service-switch %mdns-host-lookup-nss))

