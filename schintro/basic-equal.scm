;;;; eq? is useful for fast identity (same object) comparisons of non-numbers,
;;;; = performs numeric comparisons on numbers,
;;;; eqv? is like eq?, but treats copies of the same number as though they were the same object, and
;;; equal? performs a "deep" comparison of the structure of data structures. (It uses eqv? for components that are numbers.)

(define (our-equal? a b)
  (cond ((eqv? a b)
         #t)
        ((and (pair? a)
              (pair? b)
              (our-equal? (car a) (car b))
              (our-equal? (cdr a) (cdr b)))
         #t)
        (else
         #f)))

(define (first-two-eqv? target thing)
   (and (eqv? (car target) (car thing))
        (eqv? (cadr target) (cadr thing))))

(define (first-two-eqv? target thing)
   (and (eqv? (car target) (car thing))
        (eqv? (cadr target) (cadr thing))))

(define (mem test-proc thing lis)
  (if (null? lis)
      #f
      (if (test-proc (car lis) thing)
          lis
          (mem test-proc thing (cdr lis)))))

(define (member thing lis)
  (mem equal? thing lis))

(define (mem-first-two? thing lis)
  (mem first-two-eqv? thing lis))

(define (mem-first-two thing lis)
   (let ((first-two-eqv? (lambda (target thing)
                            (and (eqv? (car target) (car thing))
                                 (eqv? (cadr target) (cadr thing))))))
     (mem first-two-eqv? thing lis)))

(define (mem-first-two thing lis)
   (mem (lambda (target thing)
           (and (eqv? (car target) (car thing))
                (eqv? (cadr target) (cadr thing))))
        target
        lis))

(define (make-mem-proc pred?)
   (lambda (target lis)
      (mem pred? target lis)))

(define member (make-mem-proc equal?))
(define memq   (make-mem-proc eq?))
(define memv   (make-mem-proc eqv?))

(define (make-mem-proc pred?)
   (letrec ((mem-proc (lambda (thing lis)
                         (if (null? lis)
                             #f
                             (if (pred? (car lis) thing)
                                 lis
                                 (mem-proc pred? thing (cdr lis)))))))
     mem-proc))
