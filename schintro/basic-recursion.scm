;;;; some examples from the book
;;;; https://docs.scheme.org/schintro

(define (length lis)
   (cond ((null? lis)
          0)
         ((pair? lis)
          (+ 1 (length (cdr lis))))
         (else
          (error "invalid argument to length"))))

(define (pair-copy pr)
  (cons (car pr) (cdr pr)))

(define (pair-tree-deep-copy thing)
   (if (not (pair? thing))
       thing
       (cons (pair-tree-deep-copy (car thing))
             (pair-tree-deep-copy (cdr thing)))))

(define (list-copy lis)
   (cond ((null? lis)
          '())
         (else
          (cons (car lis)
                (list-copy (cdr lis))))))

(define (append2 lis1 lis2)
  (cond ((null? lis1)
         lis2)
        (else
         (cons (car lis1)
               (append2 (cdr lis1) lis2)))))

(define (reverse lis)
   (if (null? lis)
       '()
       (append (reverse (cdr lis))
               (list (car lis)))))

(define (list-each proc lis)
  (cond ((null? lis)
         #f)
        (else
         (proc (car lis))
         (list-each proc (cdr lis)))))

(define (map proc lis)
  (cond ((null? lis)
         '())
        ((pair? lis)
         (cons (proc (car lis))
               (map proc (cdr lis))))))

(define (for-each1 proc lis)
  (cond ((null? (cdr lis))  ; one-element list?
         (proc (car lis)))
        (else
         (proc (car lis))
         (for-each1 proc (cdr lis)))))

(define (member thing lis)
  (if (null? lis)
      #f
      (if (equal? (car lis) thing)
          lis
          (member thing (cdr lis)))))

(define (assoc thing alist)
  (if (null? alist)
      #f
      (if (equal? (car (car alist)) thing)
          (car alist)
          (assoc thing (cdr alist)))))

(define (mem test-proc thing lis)
  (if (null? lis)
      #f
      (if (test-proc (car lis) thing)
          lis
          (mem test-proc thing (cdr lis)))))

(define (member thing lis)
  (mem equal? thing lis))
