;;;; A record is an object with named fields.
;;;; a simple record definition facility written in Scheme

;;; we can use Scheme vectors to represent points,
;;; with each point represented as a small vector,
;;; with a slot for the x field and a slot for the y field.


;; a point is represented as a three-element vector, with the 0th
;; slot holding the symbol point, the 1st slot representing
;; the x field,, and the 2nd slot representing the y field.
(define (make-point x y)
  (vector 'point x y))

;; check to see if something is a point by checking to see if it's
;; a vector whose 0th slot holds the symbol point.
(define (point? obj)
  (and (vector? obj)
       (eq? (vector-ref obj 0) 'point)))

;; accessors to get and set the value of a point's x field.
(define (point-x obj)
   (if (point? obj)
       (vector-ref obj 1)
       (error "attempt to apply point-x to a non-point.")))

(define (point-x-set! obj value)
  (vector-set! obj 1 value))

;; accessors to get and set the value of a point's y field.
(define (point-y obj)
   (if (point? obj)
       (vector-ref obj 2)
       (error "attempt to apply point-y to a non-point.")))

(define (point-y-set! obj value)
  (vector-set! obj 2 value))

;; define-struct is a macro that takes a struct name and any number of field
;; names, all of which should be symbols.  Then it generates a begin expression
;; to be compiled, where the begin expression contains the constructor for this
;; structure type, a predicate to identify instances of this structure type,
;; and all of the accessor definitions for its fields.
(define-macro (define-struct struct-name . field-names)
  ; analyze the macro call expression and construct some handy symbols
  ; and an s-expression that will define and record the accessor methods.
  (let* ((maker-name (string->symbol
                      (string-append "make-"
                                     (symbol->string struct-name))))
         (pred-name (string->symbol
                     (string-append (symbol->string struct-name) "?")))
         (accessor-defns (generate-accessor-defns struct-name field-names)))
           ; return an s-expression that's a series of definitions to be
           ; interpreted or compiled.
    `(begin (define (,maker-name ,@field-names)
              (vector ',struct-name ,@field-names))
            (define (,pred-name obj)
              (and (vector? obj)
                   (eq? (vector-ref obj 0) ,struct-name)))
            ,@accessor-defns)))

;; generate-accessor-defns generates a list of s-expressions that
;; define the accessors (getters and setters) for a structure.
(define (generate-accessor-defns structname fnames)
  (let ((structname-string (symbol->string structname)))
    ; loop over the fieldnames, and for each fieldname, generate two
    ; s-expressions:  one that is a definition of a getter, and one that's
    ; a definition of a setter.
    ; As we loop, increment a counter i so that we can use it as the index
    ; for each slot we're generating accessors for

    (let loop ((fieldnames fnames)
               (i 1))
      (if (null? fieldnames)
          '()
             ; take a fieldname symbol, convert to string, append it to the
             ; struct name string with a hyphen in the middle, and convert
             ; that to a symbol...
          (let* ((fieldname-string (symbol->string (car fieldnames)))
                 (getter-name (string->symbol
                               (string-append structname-string
                                              "-"
                                              fieldname-string)))
                 (setter-name (string->symbol
                               (string-append structname-string
                                              "-"
                                              fieldname-string
                                              "-set!"))))

                 ; now construct the define forms and cons them onto the
                 ; front of the list of the remaining define forms, generated
                 ; iteratively (tail-recursively)

            (cons `(define (,getter-name obj)
                     (vector-ref obj ,i))
                  (cons `(define (,setter-name obj value)
                           (vector-set! obj ,i value))
                        (loop (cdr fieldnames)
                              (+ i 1)))))))))
