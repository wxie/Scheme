;;;; named let

(let loop ((i 0))
   (display i)
   (if (< i 10)
       (loop (+ i 1))))

;;; same as the following
(letrec ((loop (lambda (i)      ; define a recursive
                  (display i)   ; procedure whose body
                  (if (< i 10)  ; is the loop body
                      (loop (+ i 1))))))
  (loop 0)) ;; start the recursion with 0 as arg i

(define (property-list-search lis target)
   (let loop ((l lis))
      (cond ((null? l)
             #f)
            ((eq? (car l) target)
             (cadr l))
            (#t
             (loop (cddr l))))))

;;; same as
(define (property-list-search lis target)
   (letrec ((loop (lambda (l)
                     (cond ((null? l)
                            #f
                           ((eq? (car l) target)
                            (cadr l))
                           (#t
                            (loop (cddr l))))))))
      (loop lis)))   ;; start the recursion
