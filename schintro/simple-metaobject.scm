;;;; The two most important kinds of metaobjects are class objects and generic procedure objects.
;;;; A class object represents instances of a particular class,
;;;; and a generic procedure object represents a generic operation.

(define (generate-slots-alist slot-decls slot-num)
  (if (null? slot-decls)
      '()
      (cons `(,(caar slot-decls)
              ,(slot-n-getter slot-num)
              ,(slot-n-setter slot-num))
            (generate-slots-alist (cdr slot-decls)
                                  (+ 1 slot-num)))))

(define (slot-n-getter offset)
  (lambda (obj)                        ; return a procedure to read
    (vector-ref obj offset)))         ; slot n of an object
(define (slot-n-setter offset)
  (lambda (obj value)                  ; return a procedure to update
    (vector-set! obj offset value)))  ; slot n of an object

(define-macro (define-class class-name superclass-list . slot-decls)
  `(define ,class-name
     (let ((slots-slist (generate-slots-alist ',slot-decls 1)))

                                        ; create the class object, implemented as a Scheme vector
       (let ((class-object (vector <<class>>            ; metaclass
                                   ',class-name         ; name
                                   (list ,@superclass-list) ; supers
                                   slots-alist          ; slots
                                   '*dummy*)))          ; creator

                                        ; install a routine to create instances
         (vector-set! class-object
                      4
                                        ; creation routine takes slot values
                                        ; as args, creates a vector w/class
                                        ; pointer for this class followed by
                                        ; slot values in place.
                      (lambda ,(map car slot-decls)
                        (vector class-object
                                ,@(map car slot-decls))))


                                        ; register accessor methods with appropriate generic procs
         (register-accessor-methods class-object slots-alist)

         class-object))))

(define <<class>>
        (let ((the-object (vector '*dummy*   ; placeholder for class ptr
                                  '<<class>> ; name of this class
                                  '()        ; superclasses (none)
                                  '()        ; slots (none)
                                  #f         ; allocator (none)
                                  '())))     ; prop. list (initially empty)
           ; set class pointer to refer to itself
           (vector-set! the-object 0 the-object)
           ; and return the class object as initial value for define
           the-object))

; An object is an instance of a class if it's represented as a
; Scheme vector whose 0th slot holds a class object.
; Note: we assume that we never shove class objects into other
;       vectors.  We could relax this assumption, but our code
;       would be slower.
(define (instance? obj)
   (and (vector? obj)
        (class? (vector-ref 0 obj))))

; An object is a class (meta)object if it's represented as a Scheme
; vector whose 0th slot holds a pointer to the class <<class>>.
; Note: we assume that we never shove the <<class>> object into
;       other vectors.  We could relax this, at a speed cost.
(define (class? obj)
   (and (vector? obj)
        (eq? (vector-ref 0 obj) <<class>>)))

; We can fetch the class of an instance by extracting the value
; in its zeroth slot.  Note that we don't check that the argument
; obj *is* an instance, so applying this to a non-instance is an error.
(define (class-of-instance obj)
   (vector-ref obj 0))

(define-macro (define-generic name . args)
   `(define ,name
            (let ((method-alist '()))
               (lambda (,@args)
                 (let* ((class (class-of-instance ,(car args))))
                        (method (cadr (assq class method-alist))))
                    (if method
                        (method obj item)
                        (error "method not found"))))))

(define *generic-procedures* '())

(define-macro (define-generic name . args)
   `(define ,name
            (letrec ((gp-name ,name)
                     (method-alist '())
                     (method-adder
                      (lambda (generic-proc method)
                            (set! method-alist
                                  (cons (list generic-proc method)
                                        method-alist))))
                     (generic-proc
                      (lambda (,@args)
                         (let* ((class (class-of-instance ,(car args))))
                                (method (cadr (assq class
                                                    method-alist))))
                            (if method
                                (method obj item)
                                (error "method not found for "
                                       gp-name))))))

               ; add the generic procedure and its method-adding
               ; routine to the association list of generic procedures

               (set! *generic-procedures*
                     (cons (list generic-proc method-adder)
                           *generic-procedures*))

               generic-procedure))

(define-macro (define-method gp (((arg1 class) . args) . body))
   `(add-method-to-generic-proc ,gp
                                ,class
                                (lambda (arg1 ,@args)
                                   ,@body)))
