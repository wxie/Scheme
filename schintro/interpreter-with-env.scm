;;;; a new interpreter for a bigger subset of Scheme; it handles all of the essential special forms of Scheme, except for macro definitions.

(define (eval expr envt)
   (cond ((symbol? expr)
          (eval-symbol expr envt))
         ((pair? expr)
          (eval-list expr envt))
         ((self-evaluating? expr)
          expr)
         (#t
          (error "Illegal expression form" expr))))

(define (eval-list list-expr envt)
   ;; only try to consider it specially if the head is a symbol
   (if (symbol? (car list-expr))

       ;; look it up in the current lexical environment
       (let ((binding-info (envt-lexical-lookup envt (car list-expr))))

          ;; switch on the type of thing that it is
          (cond ((not binding-info)
                 (error "Unbound symbol" (car list-expr)))
                (else
                 (cond
                    ;; special forms just call the special-form
                    ;; evaluator, which is stored in the binding-info
                    ;; object itself
                   ((eq? (binding-type binding-info) '<special-form>)
                    ((bdg-special-form-evaluator binding-info) list-expr
                                                               envt))

                   ((eq? (binding-type binding-info) '<variable>)
                    (eval-combo (bdg-variable-ref binding-info)
                                (cdr list-expr)
                                envt))
                   ((eq? (binding-type binding-info) '<syntax>)
                    (eval-macro-call (bdg-syntax-transformer binding-info)
                                     list-expr
                                     envt))
                   (else
                    (error "Unrecognized binding type"))))))

         ;; the head of the list is not a symbol, so evaluate it
         ;; and then do an eval-combo to evaluate the args and
         ;; call the procedure
         (eval-combo (eval (car list-expr) envt)
                     (cdr list-expr)
                     envt)))

(define (eval-let let-form envt)
  ;; extract the relevant portions of the let form
  (let ((binding-forms (cadr let-form))
        (body-forms (cddr let-form)))

    ;; break up the bindings part of the form
    (let ((var-list (map car binding-forms))
          (init-expr-list (map cadr binding-forms)))

      ;; evaluate initial value expressions in old envt, create a
      ;; new envt to bind values,
      (let ((new-envt (make-envt var-list
                                 (eval-multi init-expr-list envt)
                                 envt)))
        ;; evaluate the body in new envt
        (eval-sequence body-forms new-envt)))))

(define (eval-multi arg-forms envt)
   (map (lambda (x)
           (eval x envt))
        arg-forms))

(define (eval-sequence arg-forms envt)
   (if (pair? arg-forms)
       (cond ((pair? (cdr arg-forms))
              (eval (car arg-forms) envt)
              (eval-sequence (cdr arg-forms) envt))
             (else
              (eval (car arg-forms) envt)))
       '*undefined-value*)) ; the value of an empty sequence

(define (eval-symbol name-symbol envt)
   (let ((binding-info (envt-lexical-lookup envt name-symbol)))
      (cond ((not binding-info)
             (error "Unbound variable" name-symbol))
            ((eq? (binding-type binding-info) '<variable>)
             (bdg-variable-ref binding info))
            (else
             (error "non-variable name referenced as variable"
                    name-symbol)))))

(define (eval-set! set-form envt)
   (let ((name (cadr set-form))
         (value-expr (caddr set-form)))
      (let ((binding-info (envt-lexical-lookup envt name)))
         (cond ((not binding-info)
                (error "Attempt to set! unbound variable" name))
               ((eq? (binding-type binding-info) '<variable>)
                (bdg-variable-set! binding-info (eval value-expr envt)))
               (else
                (error "Attempt to set! a non-variable" name))))))

(define (eval-lambda lambda-form envt)
   (let ((formals (cadr lambda-form))
         (body (cddr lambda-form)))
     (make-closure envt formals body)))

(define (eval-combo proc arg-expr-list envt)
   ;; use our own kind of apply to run our own kind of closures
   (eval-apply proc
               ;; evaluate the arguments, collecting results into a list
               (eval-multi arg-expr-list
                          envt)))

(define (eval-apply proc arg-list)
   (if (primitive-closure? proc)

       ;; it's a primitive, so extract the underlying language's
       ;; closure for the primitive, and do a real (underlying Scheme)
       ;; apply to call it
       (apply (closure-primitive proc) arg-list)

       ;; it's not a primitive closure, so it must be something
       ;; we created with make-closure
       ;;
       ;; first, bind the actuals into a new environment, which
       ;; is scoped inside the environment in which the closure
       ;; was closed
       (let ((new-envt (make-envt (closure-args proc)
                                  arg-list
                                  (closure-envt proc))))
          ;; then, evaluate the body forms, returning the
          ;; value of the last of them.
          (eval-sequence (closure-body proc)
                         new-envt))))
