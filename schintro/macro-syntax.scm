;;;; syntax extensions, also known as macros.

(define-syntax or
   (syntax-rules ()
      ((or)             ; OR of zero arguments
       #f)              ;   is always false
      ((or a)           ; OR of one argument
       a)               ;   is equivalent to the argument expression
      ((or a b c ...)   ; OR of two or more arguments
       (let ((temp a))  ;   is the first or the OR of the rest
          (if temp
              temp
              (or b c ...))))))

(define-syntax let
   (syntax-rules ()
     ((_ ((var value-expr) ...) body-expr ...)  ; pattern
      ((lambda (var ...)
          body-expr ...)
       (value-expr ...)))))

(define-syntax let*
   (syntax-rules ()
      ((_ () body-expr ...)
       (begin body-expr ...))
      ((_ ((var1 value-expr1)(var value-expr) ...))
       (let ((var1 value-expr1))
          (_ ((var value-expr) ...))))))

(define (quasiquote1 expr)
  (if (not (pair? expr)) ; if quoted expr is not a list, it's just
      expr               ; a literal
                         ; else we'll grab a subexpression and cons it (appropriately
                         ; quoted or not) onto the result of recursively quasiquoting
                         ; the remaining arguments
      (let ((first-subexpr (car expr))
            (rest-subexprs (cdr expr)))
        (if (and (pair? first-subexpr)
                 (eq? (car first-subexpr) 'unquote))
            (list 'cons
                  first-subexpr       ; gen expr to eval this subexpr
                  (quasiquote1 rest-subexprs))
            (list 'cons
                  (list 'quote first-subexpr)    ; quote this subexpr
                  (quasiquote1 rest-subexprs))))))

(define-macro (quasiquote expr)
   (quasiquote1 expr))

;;;; ERROR - Wrong type to apply: let
;;; variable arity OR with subtle scope bug
(define-macro (or . args)
   (if (null? args)  ; zero arg or?
       #f
       (if (null? (cdr? args)) ; one arg or?
           (car args)
           `(let ((temp ,(car args)))
               (if temp
                   temp
                   (or ,@(cdr args)))))))

;;; Version of 2-arg OR with scope bug fixed
(define-macro (or first-arg second-arg)
   (let ((temp-name (generate-symbol)))
      `(let ((,temp-name ,first-arg))
          (if ,temp-name
              ,temp-name
              ,second-arg))))
