;;;; tail recursion avoids saving a lot of caller stach data

;; a tail-recursive list summing procedure
(define (loop lis sum-so-far)
  (cond ((null? lis)
         sum-so-far)
        (else
         (loop (cdr lis)
               (+ sum-so-far (car lis))))))

;; a friendly wrapper that supplies an initial running sum of 0
(define (list-sum lis)
  (loop lis 0))

(define (list-sum lis)
  ;; define a local, tail-recursive list summing procedure
  (letrec ((loop (lambda (lis sum-so-far)
                   (cond ((null? lis)
                          sum-so-far)
                         (else
                          (loop (cdr lis)
                                (+ sum-so-far (car lis))))))))
    (loop lis 0))) ;; start off recursive summing with a sum of 0

(define (list-sum lis)
  (let loop ((lis lis)
             (sum-so-far 0))
    (cond ((null? lis)
           sum-so-far)
          (else
           (loop (cdr lis)
                 (+ sum-so-far (car lis)))))))

(define (length lis)
  (letrec ((len (lambda (lis length-so-far)
                  (if (null? lis)
                      length-so-far
                      (len (cdr lis)
                           (+ 1 length-so-far))))))
    (len lis 0)))

(define (reduce fn base-value lis)
  (if (null? lis)
      base-value
      (fn (car lis)
          (reduce fn base-value (cdr lis)))))

(define (list-copy lis)
  (reduce cons '() lis))

(define (make-reducer fn base-value)
  (lambda (lis)
    (reduce fn base-value lis)))

(define (make-reducer fn base-value)
  (letrec ((reduce (lambda (lis)
                     (if (null? lis)
                         base-value
                         (fn (car lis)
                             (reduce (cdr lis)))))))
    reduce)) ;; return new closure of local procedure

(define (make-reducer fn base-value)
  (define (reduce lis)
    (if (null? lis)
        base-value
        (fn (car lis)
            (reduce (cdr lis)))))
  reduce)  ;; return new closure of local procedure

(define list-sum (make-reducer + 0))
(list-sum '(1 2 3 4))
