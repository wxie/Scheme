;;;; read-eval-print-loop
;;;; bash repl as example
;;;; bash_macros(){ while read -r i; do eval "$( printf "%s" "$i" | awk "$1" ; )" ; done ; } ;

(define (rep-loop)
   (display "repl>")      ; print a prompt
   (write (eval (read)))  ; read expr., pass to eval, write result
   (rep-loop))            ; loop (tail-recursive call) to do it again

(define (rep-loop)
   (display "repl>")              ; print a prompt
   (let ((expr (read)))           ; read an expression, save it in expr
      (cond ((eq? expr 'halt)     ; user asked to stop?
             (display "exiting read-eval-print loop")
             (newline))
            (#t                   ; otherwise,
             (write (eval expr))  ;  evaluate and print
             (newline)
             (rep-loop)))))       ;  and loop to do it again

(define (rep-loop evaluator)
   (display "repl>")                 ; print a prompt
   (let ((expr (read)))              ; read an expression, save it in expr
      (cond ((eq? expr 'exit)        ; user asked to stop?
             (display "exiting read-eval-print loop")
             (newline))
            (#t                      ; otherwise,
             (write (evaluator expr)) ;  evaluate and print
             (rep-loop evaluator))))) ;  and loop to do it again

;;; a scanner for a simple subset of the lexical syntax of Scheme
(define (read-token)
   (let ((first-char (read-char)))
      (cond ;; if first-char is a space or line break, just skip it
            ;; and loop to try again by calling self recursively
            ((char-whitespace? first-char)
             (read-token))
            ;; else if it's a left paren char, return the special
            ;; object that we use to represent left parenthesis tokens.
            ((eq? first-char #\( )
             left-paren-token)
            ;; likewise for right parens
            ((eq? first-char #\) )
             right-paren-token)
            ;; else if it's a letter, we assume it's the first char
            ;; of a symbol and call read-identifier to read the rest of
            ;; of the chars in the identifier and return a symbol object
            ((char-alphabetic? first-char)
             (read-identifier first-char))
            ;; else if it's a digit, we assume it's the first digit
            ;; of a number and call read-number to read the rest of
            ;; the number and return a number object
            ((char-numeric? first-char)
             (read-number first-char))
            ;; else it's something this little reader can't handle,
            ;; so signal an error
            (else
             (error "illegal lexical syntax")))))

;;; whitespace? checks whether char is either space or newline
(define (char-whitespace? char)
  (or (eq? char #\space)
      (eq? char #\newline)))

(define (left-parenthesis-token? thing)
   (eq? thing left-parenthesis-token))
(define (right-parenthesis-token thing)
  (eq? thing right-parenthesis-token))

;;; read-identifier reads an identifier and returns a symbol
;;; to represent it
(define (read-identifier chr)

  ;; read-identifier-helper reads in one character a time and puts it into
  ;; a list. If it finds the character is a finishing character, then
  ;; it reverses the list and returns.

  (define (read-identifier-helper list-so-far)
    (let ((next-char (peek-char)))
      ;; if next-char is a letter or a digit then call self recursively
      (if (or (char-alphabetic? next-char)
              (char-numeric? next-char))
          (read-identifier-helper (cons (read-char) list-so-far))
          ;; else return list we've read, reversing it into proper order
          (reverse list-so-far))))

  ;; call read-identifier-helper to accumulate the characters in the
  ;; identifier, then convert that to a string object and convert *that*
  ;; to a symbol object.
  ;; Note that string->symbol ensures that only one symbol with a given
  ;; printname string is ever constructed, so there are no duplicates.

  (string->symbol (list->string (read-identifier-helper (list chr)))))

;;; read-number reads a sequence of digits and constructs a Scheme number
;;; object to represent it.  Given the first character, it reads one
;;; char at a t time and checks to see if it's a digit.  If so, it
;;; conses it onto a list of numbers read so far.  Otherwise, it
;;; reverses the list of digits, converts it to a string, and converts
;;; that to a Scheme number object.
(define (read-number chr)
  (define (read-number-helper list-so-far)
    (let ((next-char (peek-char)))
      ;; if next-char is a digit then call self resursively
      (if (char-numeric? next-char)
          (read-number-helper (cons (read-char) list-so-far))
          ;; else return the list we've read, reversing into proper order
          (reverse list-so-far))))
  ;; read the string of digits, convert to string, convert to number
  (string->number (list->string (read-number-helper (list chr)))))

;;; Simplified version of read for subset of Scheme s-expression syntax
(define (micro-read)
   (let ((next-token (read-token)))
      (cond ((token-leftpar? next-token)
             (read-list '()))
            (else
             next-token))))

(define (read-list list-so-far)
   (let ((token (micro-read-token)))
      (cond ((token-rightpar? token)
             (reverse list-so-far))
            ((token-leftpar? token)
             (read-list (cons (read-list '()) list-so-far)))
            (else
             (read-list (cons token list-so-far))))))

(define (math-eval expr)
  (cond ;; self-evaluating object?  (we only handle numbers)
        ((number? expr)
         expr)
        ;; compound expression? (we only handle two-arg combinations)
        (else
         (math-eval-combo expr))))

(define (math-eval-combo expr)
  (let ((operator-name (car expr))
        (arg1 (math-eval (cadr expr)))
        (arg2 (math-eval (caddr expr))))
     (cond ((eq? operator-name 'plus)
            (+ arg1 arg2))
           ((eq? operator-name 'minus)
            (- arg1 arg2))
           ((eq? operator-name 'times)
            (* arg1 arg2))
           ((eq? operator-name 'quotient)
            (/ arg1 arg2))
           (else
            (error "Invalid operation in expr:" expr)))))

(define (eval expr)
   (cond ;; variable reference
         ((symbol? expr)
          (eval-variable expr))
         ;; combination OR special form (any parenthesized expr)
         ((pair? expr)
          (eval-list expr))
         ;; any kind of self-evaluating object
         ((self-evaluating? expr)
          expr)
         (else
          (error "Illegal expression: " expr))))

(define (eval-list expr)
   (if (and (symbol? (car expr))
            (special-form-name? (car expr)))
       (eval-special-form expr)
       (eval-combo)))

(define (eval-special-form expr)
  (let ((name (car expr)))
     (cond ((eq? name 'define)
            (eval-define expr))
           ((eq? name 'set!)
            (eval-set! expr))
           ((eq? name 'if)
            (eval-if expr)))))

(define (eval-if expr)
   (let ((expr-length (length expr)))
     (if (eval (cadr expr))
         (eval (caddr expr))
         (if (= expr-length 4)
             (eval (cadddr expr))
             #f))))

(define t-l-envt '())

(define (toplevel-bind! name value)
   (let ((bdg (assoc name t-l-envt))) ;; search for association of name
      ;; if binding already exists, put new value (in cadr) of association
      ;; else create a new association with given value
      (if bdg
          (set-car! (cdr bdg) value)
          (set! t-l-envt
                (cons (list name value) t-l-envt)))))

(toplevel-bind! '+ +)
(toplevel-bind! '- -)
(toplevel-bind! '* *)
(toplevel-bind! '/ /)

(define (toplevel-get name)
   (cadr (assoc name t-l-envt)))

(define (toplevel-set! name value)
   (set-car! (cdr (assoc name t-l-envt))
             value))

(define (eval-define! expr)
   (toplevel-bind! (cadr expr)
                   (eval (caddr expr))))

(define (eval-set! expr)
   (toplevel-set! (cadr expr)
                  (eval (caddr expr))))
