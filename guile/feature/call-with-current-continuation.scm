;;;; (define call/cc call-with-current-continuation)

;;; call/cc takes a procedure, when the procedure is
;;; called call/cc will return with the parameter of
;;; the procedure called.

(define (search wanted lst)
  "Return the first element in LST for which WANTED"
  (call/cc
   (lambda (return)
     (for-each (lambda (element)
                 (if (equal? wanted element)
                     (return element)))
               lst)
     #f)))

;;; Call LIKE-IT with a custom argument when we like ELEMENT.
(define (treat element like-it)
  (if (equal? 'good element)
      (like-it 'got-good)))

;;; Call TREAT with every element in the LIST and a procedure to call
;;; when TREAT likes this element.
(define (search treat lst)
  (call/cc
   (lambda (return)
     (for-each (lambda (element)
                 (treat element return))
               lst)
     #f)))

;;; in and out
(define (hefty-computation do-other-stuff)
  (let loop ((n 5))
    (display "Hefty computation: ")
    (display n)
    (newline)
    (set! do-other-stuff (call/cc do-other-stuff))
    (display "Hefty computation (b)")
    (newline)
    (set! do-other-stuff (call/cc do-other-stuff))
    (display "Hefty computation (c)")
    (newline)
    (set! do-other-stuff (call/cc do-other-stuff))
    (if (> n 0)
        (loop (- n 1)))))

;; notionally displays a clock
(define (superfluous-computation do-other-stuff)
  (let loop ()
    (for-each (lambda (graphic)
                (display graphic)
                (newline)
                (set! do-other-stuff (call/cc do-other-stuff)))
              '("Straight up." "Quarter after." "Half past."  "Quarter til."))
    (loop)))

;; (hefty-computation superfluous-computation)

;;; versatility of call/cc
(define r #f)

(+ 1 (call/cc
       (lambda (k)
         (set! r k)
         (+ 2 (k 3)))))
;; => 4

(r 5)
;; => 6

(+ 3 (r 5))
;; => 6

;;; quick list product to 0
(define list-product
  (lambda (s)
    (call/cc
      (lambda (exit)
        (let recur ((s s))
          (if (null? s) 1
              (if (= (car s) 0) (exit 0)
                  (* (car s) (recur (cdr s))))))))))
