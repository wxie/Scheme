;;;; Nondeterminism ;;;;

;;; amb takes zero or more expressions, and makes
;;; a nondeterministic (or “ambiguous”) choice
;;; among them, preferring those choices that
;;; cause the program to converge meaningfully.

(define amb-fail '*)

(define initialize-amb-fail
  (lambda ()
    (set! amb-fail
      (lambda ()
        (error "amb tree exhausted")))))

(initialize-amb-fail)

(define-macro amb
  "A call to amb first stores away, in +prev‑amb‑fail,
the amb‑fail value that was current at the time of entry.
This is because the amb‑fail variable will be set to
different failure continuations as the various alternates
are tried.

We then capture the amb’s entry continuation +sk, so that
when one of the alternates evaluates to a non-failing
value, it can immediately exit the amb.

Each alternate alt is tried in sequence (the implicit-begin
sequence of Scheme).

First, we capture the current continuation +fk, wrap it in
a procedure and set amb‑fail to that procedure. The
alternate is then evaluated as (+sk alt). If alt evaluates
without failure, its return value is fed to the continuation
+sk, which immediately exits the amb call. If alt fails, it
calls amb‑fail. The first duty of amb‑fail is to reset
amb‑fail to the value it had at the time of entry. It then
invokes the failure continuation +fk, which causes the next
alternate, if any, to be tried.

If all alternates fail, the amb‑fail at amb entry, which we
had stored in +prev‑amb‑fail, is called."
  (lambda alts...
    `(let ((+prev-amb-fail amb-fail))
       (call/cc
        (lambda (+sk)

          ,@(map (lambda (alt)
                   `(call/cc
                     (lambda (+fk)
                       (set! amb-fail
                         (lambda ()
                           (set! amb-fail +prev-amb-fail)
                           (+fk 'fail)))
                       (+sk ,alt))))
                 alts...)

          (+prev-amb-fail))))))

(define-macro bag-of
  "bag‑of first saves away its entry amb‑fail.
 It redefines amb‑fail to a local continuation
 +k created within an if-test. Inside the test,
 the bag‑of argument e is evaluated. If e
 succeeds, its result is collected into a list
 called +results, and the local continuation is
 called with the value #t. This causes the
 if-test to succeed, causing e to be retried at
 its next backtrack point. More results for e
 are obtained this way, and they are all
 collected into +results.

 Finally, when e fails, it will call the base
 amb‑fail, which is simply a call to the local
 continuation with the value #f. This pushes
 control past the if. We restore amb‑fail to
 its pre-entry value, and return the +results.
 (The reverse! is simply to produce the results
 in the order in which they were generated.)"
  (lambda (e)
    `(let ((+prev-amb-fail amb-fail)
           (+results '()))
       (if (call/cc
            (lambda (+k)
              (set! amb-fail (lambda () (+k #f)))
              (let ((+v ,e))
                (set! +results (cons +v +results))
                (+k #t))))
           (amb-fail))
       (set! amb-fail +prev-amb-fail)
       (reverse! +results))))

;;; Kalotan puzzle ;;;
;;; The Kalotans are a tribe with a peculiar quirk.
;;; Their males always tell the truth. Their females
;;; never make two consecutive true statements, or
;;; two consecutive untrue statements.

;;; An anthropologist (let’s call him Worf) has begun
;;; to study them. Worf does not yet know the Kalotan
;;; language. One day, he meets a Kalotan (heterosexual)
;;; couple and their child Kibi. Worf asks Kibi: “Are
;;; you a boy?” Kibi answers in Kalotan, which of course
;;; Worf doesn’t understand.

;;; Worf turns to the parents (who know English) for
;;; explanation. One of them says: “Kibi said: ‘I am a
;;; boy.’ ” The other adds: “Kibi is a girl. Kibi lied.”

;;; Solve for the sex of the parents and Kibi.
(define assert
  (lambda (pred)
    (if (not pred) (amb))))

(define distinct?
;  "returns true if all the elements in its argument
;list are distinct, and false otherwise."
  (lambda (ls)
    (cond ((null? ls) #t)
          ((member (car ls) (cdr ls)) #f)
          (else (distinct? (cdr ls))))))

(define xor
;  "returns true if only one of its two arguments
; is true, and false otherwise."
  (lambda (a b)
    (if (or (and a (not b)) (and b (not a)))
        #t
        #f)))

(define s-k-p ;solve-kalotan-puzzle
  (lambda ()
    (let ((parent1 (amb 'm 'f))
          (parent2 (amb 'm 'f))
          (kibi (amb 'm 'f))
          (kibi-self-desc (amb 'm 'f))
          (kibi-lied? (amb #t #f)))
      (assert
       (distinct? (list parent1 parent2)))
      (assert
       (if (eqv? kibi 'm)
           (not kibi-lied?)))
      (assert
       (if kibi-lied?
           (xor
            (and (eqv? kibi-self-desc 'm)
                 (eqv? kibi 'f))
            (and (eqv? kibi-self-desc 'f)
                 (eqv? kibi 'm)))))
      (assert
       (if (not kibi-lied?)
           (xor
            (and (eqv? kibi-self-desc 'm)
                 (eqv? kibi 'm))
            (and (eqv? kibi-self-desc 'f)
                 (eqv? kibi 'f)))))
      (assert
       (if (eqv? parent1 'm)
           (and
            (eqv? kibi-self-desc 'm)
            (xor
             (and (eqv? kibi 'f)
                  (eqv? kibi-lied? #f))
             (and (eqv? kibi 'm)
                  (eqv? kibi-lied? #t))))))
      (assert
       (if (eqv? parent1 'f)
           (and
            (eqv? kibi 'f)
            (eqv? kibi-lied? #t))))
      (list parent1 parent2 kibi))))

;; (s-k-p)
;; => (f m f)

;;; Map coloring ;;;
;;; The following program solves the problem of
;;; coloring a map of Western Europe.

;;; In our solution, we create for each country a
;;; data structure. The data structure is a
;;; 3-element list: The first element of the list
;;; is the country’s name; the second element is
;;; its assigned color; and the third element is
;;; the colors of its neighbors. Note we use the
;;; initial of the country for its color variable.
;;; E.g., the list for Belgium is
;;; (list 'belgium b (list f h l g)),
;;; because — per the problem statement — the
;;; neighbors of Belgium are
;;; France, Holland, Luxembourg, and Germany.

;;; Once we create the lists for each country, we
;;; state the (single!) condition they should
;;; satisfy, viz., no country should have the
;;; color of its neighbors. In other words, for
;;; every country list, the second element should
;;; not be a member of the third element.
(define choose-color
  (lambda ()
    (amb 'red 'yellow 'blue 'white)))

(define c-u  ; color-europe
  (lambda ()

    ;choose colors for each country
    (let ((p (choose-color)) ;Portugal
          (e (choose-color)) ;Spain
          (f (choose-color)) ;France
          (b (choose-color)) ;Belgium
          (h (choose-color)) ;Holland
          (g (choose-color)) ;Germany
          (l (choose-color)) ;Luxemb
          (i (choose-color)) ;Italy
          (s (choose-color)) ;Switz
          (a (choose-color)) ;Austria
          )

      ;construct the adjacency list for
      ;each country: the 1st element is
      ;the name of the country; the 2nd
      ;element is its color; the 3rd
      ;element is the list of its
      ;neighbors’ colors
      (let ((portugal
             (list 'portugal p
                   (list e)))
            (spain
             (list 'spain e
                   (list f p)))
            (france
             (list 'france f
                   (list e i s b g l)))
            (belgium
             (list 'belgium b
                   (list f h l g)))
            (holland
             (list 'holland h
                   (list b g)))
            (germany
             (list 'germany g
                   (list f a s h b l)))
            (luxembourg
             (list 'luxembourg l
                   (list f b g)))
            (italy
             (list 'italy i
                   (list f a s)))
            (switzerland
             (list 'switzerland s
                   (list f i a g)))
            (austria
             (list 'austria a
                   (list i s g))))
        (let ((countries
               (list portugal spain
                     france belgium
                     holland germany
                     luxembourg
                     italy switzerland
                     austria)))

          ;the color of a country
          ;should not be the color of
          ;any of its neighbors
          (for-each
           (lambda (c)
             (assert
              (not (memq (cadr c)
                         (caddr c)))))
           countries)

          ;output the color
          ;assignment
          (for-each
           (lambda (c)
             (display (car c))
             (display " ")
             (display (cadr c))
             (newline))
           countries))))))

;; (c-u) ;(color‑europe)
;; =>
