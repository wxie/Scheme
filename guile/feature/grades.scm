;;;; sample grade system ;;;;
(library (grades)
  (export gpa->grade gpa distribution histogram)
  (import (rnrs))

  (define in-range?
    (lambda (x n y)
      (and (>= n x) (< n y))))

  (define-syntax range-case
    (syntax-rules (- else)
      [(_ expr ((x - y) e1 e2 ...) ... [else ee1 ee2 ...])
       (let ([tmp expr])
         (cond
          [(in-range? x tmp y) e1 e2 ...]
          ...
          [else ee1 ee2 ...]))]
      [(_ expr ((x - y) e1 e2 ...) ...)
       (let ([tmp expr])
         (cond
          [(in-range? x tmp y) e1 e2 ...]
          ...))]))

  (define letter->number
    (lambda (x)
      (case x
        [(a)  4.0]
        [(b)  3.0]
        [(c)  2.0]
        [(d)  1.0]
        [(f)  0.0]
        [else (assertion-violation 'grade "invalid letter grade" x)])))

  (define gpa->grade
    (lambda (x)
      (range-case x
                  [(0.0 - 0.5) 'f]
                  [(0.5 - 1.5) 'd]
                  [(1.5 - 2.5) 'c]
                  [(2.5 - 3.5) 'b]
                  [else 'a])))

  #| without letter 'x'
  (define-syntax gpa
    (syntax-rules ()
      [(_ g1 g2 ...)
       (let ([ls (map letter->number '(g1 g2 ...))])
         (/ (apply + ls) (length ls)))]))
  |#

  (define-syntax gpa  ;; with letter 'x'
    (syntax-rules ()
      [(_ g1 g2 ...)
       (let ([ls (map letter->number
                      (filter (lambda (g) (not (eq? g 'x)))
                              '(g1 g2 ...)))])
         (/ (apply + ls) (length ls)))]))

  (define (remove-duplicates ls)
    (cond ((null? ls)
           '())
          ((member (car ls) (cdr ls))
           (remove-duplicates (cdr ls)))
          (else
           (cons (car ls) (remove-duplicates (cdr ls))))))

  (define count
    (lambda (g0 gs)
      (length (filter (lambda (g) (eq? g g0))
                      gs))))

  (define-syntax distribution
    (syntax-rules ()
      [(_ g1 g2 ...)
       (let ([ls '(g1 g2 ...)])
         (remove-duplicates
          (map list (map (lambda (g0) (count g0 ls))
                         ls)
               ls)))]))

  (define print-one
    (lambda (gn)
      (string-append
       "  "
       (symbol->string (cadr gn))
       ": "
       (make-string (car gn) #\*)
       "\n")))

  (define print
    (lambda (dist)
      (if (null? dist)
          ""
          (string-append (print-one (car dist))
                         (print (cdr dist))))))

  (define histogram
    (lambda (port dist)
      (put-string port "prints:\n")
      (put-string port
                  (print dist)))))

;;; usage ;;;
#|
(import (grades))

(gpa c a c b b)  ; ==> 2.8
(gpa->grade 2.8) ; ==> b

(distribution a b a c c c a f b a) ; ==> ((4 a) (2 b) (3 c) (0 d) (1 f))

(histogram
  (current-output-port)
  (distribution a b a c c a c a f b a))

prints:
  a: *****
  b: **
  c: ***
  d:
  f: *
|#
