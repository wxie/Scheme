;;;; Alists and tables

;;; An association list, or alist, is a Scheme list of
;;; a special format, the car of which is called a key,
;;; the cdr being the value associated with the key.
;;; The procedure call (assv k al) finds the cons cell
;;; associated with key k in alist al.

;;; define a structure called table
(defstruct table (equ eqv?) (alist '()))

(define table-get
  "the procedure table‑get to get the value (as opposed
to the cons cell) associated with a given key"
  (lambda (tbl k . d)
    (let ((c (lassoc k (table.alist tbl) (table.equ tbl))))
      (cond (c (cdr c))
            ((pair? d) (car d))))))

(define lassoc
  (lambda (k al equ?)
    (let loop ((al al))
      (if (null? al) #f
          (let ((c (car al)))
            (if (equ? (car c) k) c
                (loop (cdr al))))))))

(define table-put!
  "The procedure table‑put! is used to update a key’s
value in the given table"
  (lambda (tbl k v)
    (let ((al (table.alist tbl)))
      (let ((c (lassoc k al (table.equ tbl))))
        (if c (set-cdr! c v)
            (set!table.alist tbl (cons (cons k v) al)))))))

(define table-for-each
  "The procedure table‑for‑each calls the given procedure
on every key/value pair in the table"
  (lambda (tbl p)
    (for-each
     (lambda (c)
       (p (car c) (cdr c)))
     (table.alist tbl))))
