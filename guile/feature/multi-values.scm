;;;; multi values of scheme

;;; guile has built-in support for multi-values
(define (add-and-multiply x y)
  (values (+ x y)
          (* x y)))

(define-values (added multiplied)
  (add-and-multiply 2 5))
;; added => 7
;; mutiplied => 10
