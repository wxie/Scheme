;;;; scheme evaluator
;;; https://spritely.institute/static/papers/scheme-primer.html
(use-modules (ice-9 match))

(define (env-lookup env name)
  (match (assoc name env)
    ((_key . val)
     val)
    (_
     (error "Variable unbound:" name))))

(define (extend-env env names vals)
  (if (eq? names '())
      env
      (cons (cons (car names) (car vals))
            (extend-env env (cdr names) (cdr vals)))))

;; (define (evaluate expr env)
;;  (match expr
;;    (<MATCH-PATTERN>
;;     <MATCH-BODY> ...) ...))
(define (evaluate expr env)
  (match expr
    ;; Support builtin types
    ((or #t #f (? number?))
     expr)
    ;; Quoting
    (('quote quoted-expr)
     quoted-expr)
    ;; Variable lookup
    ((? symbol? name)
     (env-lookup env name))
    ;; Conditionals
    (('if test consequent alternate)
     (if (evaluate test env)
         (evaluate consequent env)
         (evaluate alternate env)))
    ;; Lambdas (Procedures)
    (('lambda (args ...) body)
     (lambda (. vals)
       (evaluate body (extend-env env args vals))))
    ;; Procedure Invocation (Application)
    ((proc-expr arg-exprs ...)
     (apply (evaluate proc-expr env)
            (map (lambda (arg-expr)
                   (evaluate arg-expr env))
                 arg-exprs)))))
;; end of evaluator

;;; eval-math
(define math-env
  `((+ . ,+)
    (- . ,-)
    (* . ,*)
    (/ . ,/)))

(evaluate '(* (- 8 (/ 30 5)) 21)
          math-env)
; => 42

(evaluate '((lambda (x)
              (* x x))
            4)
          math-env)
; => 16

;;; eval-fib
(define fib-program
  '((lambda (prog arg)   ; boot
      (prog prog arg))
    (lambda (fib n)      ; main program
      (if (= n 0)
          0
          (if (= n 1)
              1
              (+ (fib fib (+ n -1))
                 (fib fib (+ n -2))))))
    10))                 ; argument

(define fib-env
  `((+ . ,+)
    (= . ,=)))

(evaluate fib-program fib-env)
; => 55

;;; eva-loopup
(define (env-lookup env name)
  (match (assoc name env)
    ((_key . val)
     val)
    (_
     (error "Variable unbound:" name))))

(env-lookup '((foo . newer-foo)
              (bar . bar)
              (foo . older-foo))
            'foo)
; => 'newer-foo

;;; extended-evn
(define (extend-env env names vals)
  (if (eq? names '())
      env
      (cons (cons (car names) (car vals))
            (extend-env env (cdr names) (cdr vals)))))

(extend-env '((foo . foo-val))
            '(bar quux)
            '(bar-val quux-val))
; => ((bar . bar-val)
;     (quux . quux-val)
;     (foo . foo-val))
