;;;; scheme mutation and side-effect

(define (make-countdown n)
        (lambda ()
          (define last-n n)
          (if (zero? n)
              0
              (begin
                (set! n (- n 1))
                last-n))))

(define cdown (make-countdown 3))

;REPL> (cdown)
; => 3
;REPL> (cdown)
; => 2
;REPL> (cdown)
; => 1
;REPL> (cdown)
; => 0
;REPL> (cdown)
; => 0
