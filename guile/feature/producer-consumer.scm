;;;; producer and consumer using call/cc
;;;; usage:
;;;; (producer consumer)
;;;; output
;; P: put a peach
;; C: get a peach
;; resume from consumer: peach
;; P: put a grape
;; resume from producer: peach
;; C: get a grape
;; resume from consumer: grape
;; P: put a lemon
;; resume from producer: grape
;; C: get a lemon
;; resume from consumer: lemon

(define dish '())

(define (put! fruit) (set! dish (list fruit)))
(define (get!) (let ((fruit (car dish))) (set! dish '()) fruit))

(define (consumer back)
  (let loop()
    (if (pair? dish)
        (let ((fruit (get!)))
          (format #t "C: get a ~a~%" fruit)
          (set! back (call/cc back))
          (format #t "resume from producer: ~a~%" fruit)
          (loop)))))

(define (producer back)
  (for-each (lambda (fruit) (put! fruit)
                    (format #t "P: put a ~a~%" fruit)
                    (set! back (call/cc back))
                    (format #t "resume from consumer: ~a~%" fruit))
            '("peach" "grape" "lemon")))
