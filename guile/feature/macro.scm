;;;; scheme hygienic macro

;;; function version
(define (when test . body)
  `(if ,test
       ,(cons 'begin body)))

;;; define-macro version
(define-macro (when test . body)
  `(if ,test
       ,(cons 'begin body)))

;;; define-syntax-rule version
(define-syntax-rule (when test body ...)
  (if test
      (begin body ...)))

;; or like this
(define-syntax when
  (syntax-rules ()
    ((when test body ...)
     (if test
         (begin body ...)))))

;; in general
(define-syntax define-syntax-rule
  (syntax-rules ()
    ((define-syntax-rule (id pattern ...) template)
     (define-syntax id
       (syntax-rules ()
         ((id pattern ...)
          template))))))
