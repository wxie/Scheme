;;; a Scheme quine from madore.org
(define x '(
	    (display "(define x '(")
	    (new-line)
	    (map (lambda (s) (write s) (newline)) x)
	    (display "))")
	    (newline)
	    (display "(map eval x)")
	    (newline)
	    ))
(map eval x)
;; guile (eval ...) needs 2 arguments, not 1.
