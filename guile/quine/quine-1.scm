;;; a Scheme quine from madore.org
(define (line-write x) (write x) (newline))
(define (d l) (map line-write l))
(define (mid) (display "(do '(") (newline))
(define (end) (display "))") (newline))
(define (do l) (d l) (mid) (d l) (end))
(do '(
      (define (line-write x) (write x) (newline))
      (define (d l) (map line-write l))
      (define (mid) (display "(do '(") (newline))
      (define (end) (display "))") (newline))
      (define (do l) (d l) (mid) (d l) (end))
      ))
